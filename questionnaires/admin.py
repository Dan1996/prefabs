from django.contrib import admin
from django import forms
from questionnaires.models import *


class PageInline(admin.StackedInline):
    model = Page


class QuestionnaireAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'user']
    inlines = [PageInline]

admin.site.register(Questionnaire, QuestionnaireAdmin)

from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class Questionnaire(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('questionnaires:index')

    def __str__(self):
        return self.name


class Outcome(models.Model):
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)


class Page(models.Model):
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)


class Question(models.Model):
    text = models.TextField()
    page = models.ForeignKey(Page, on_delete=models.CASCADE)


class Answear(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    score = models.IntegerField()


class Result(models.Model):
    text = models.TextField()

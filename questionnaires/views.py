from .models import Questionnaire
from django.views.generic import ListView, View, CreateView
from django.contrib.auth.views import LoginView
from django.contrib.auth import logout
from .forms import UserForm
from django.shortcuts import redirect, render


class IndexView(ListView):
    template_name = 'questionnaires/index.html'
    context_object_name = 'all_questionnaires'

    def get_queryset(self):
        return Questionnaire.objects.all()


class UserLoginView(LoginView):
    template_name = 'questionnaires/generic_template_form.html'
    redirect_field_name = 'questionnaires:index'


def logout_view(request):
    logout(request)
    return redirect('questionnaires:index')


class UserRegistrationView(View):
    form_class = UserForm
    template_name = 'questionnaires/generic_template_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():

            user = form.save(commit=False)
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save()
            return redirect('questionnaires:index')

        return render(request, self.template_name, {'form': form})


class QuestionnaireCreate(CreateView):
    model = Questionnaire
    fields = ['name', 'description']
    template_name = 'questionnaires/generic_template_form.html'

    def form_valid(self, form):
        questionnaire = form.save(commit=False)
        questionnaire.user = self.request.user
        return super(QuestionnaireCreate, self).form_valid(form)

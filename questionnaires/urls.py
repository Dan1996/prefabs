from django.conf.urls import url
from . import views

app_name = 'questionnaires'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/', views.UserLoginView.as_view(), name='login'),
    url(r'^logout/', views.logout_view, name='logout'),
    url(r'^register/', views.UserRegistrationView.as_view(), name='register'),
    url(r'^create-questionnaire/', views.QuestionnaireCreate.as_view(), name='create-quest')
]